import React from 'react';
import Routing from './src/statemanagement/Routing';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import {persistor, store} from './src/statemanagement/utils/store';
import {SafeAreaView} from 'react-native-safe-area-context';
import {StatusBar, Text, View} from 'react-native';
import Login from './src/statemanagement/Login';

const App = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <SafeAreaView style={{flex: 1}}>
          <StatusBar barStyle={'dark-content'} />
          <Routing />
        </SafeAreaView>
      </PersistGate>
    </Provider>
  );
};
export default App;
